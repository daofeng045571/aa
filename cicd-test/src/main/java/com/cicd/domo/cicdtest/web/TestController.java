package com.cicd.domo.cicdtest.web;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName : TestController
 * @Author : liyj
 * @Date : 2020/5/31 17:14
 **/
@RestController
@RequestMapping("/test")
public class TestController {


    @GetMapping("/hello")
    public String sayHello() {
        return "hello cicd";
    }

}
